My contributions to Rosetta Code, under username Siegfried.krieger 

Contributions:

1. Bernulli numbers:
  * [local](./src/bernulli.cpp)
  * [rosetta](https://rosettacode.org/mw/index.php?title=Bernoulli_numbers&oldid=262933#Using_Boost_.7C_C.2B.2B14)

2. Average Loop:
  * [local](./src/average_loop.cpp)
  * [rosetta](https://rosettacode.org/wiki/Average_loop_length#C.2B.2B)
